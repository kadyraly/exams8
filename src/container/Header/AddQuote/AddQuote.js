import React, {Component} from 'react';
import './AddQuote.css';
import axios from '../../../axios';

class AddQuote extends Component {
    state = {
        quote: {
            text: '',
            author: ''
        },
        loading: false
    };
    quoteSubmitHandler = (e) => {
        e.preventDefault();
        this.setState({loading: true});
        axios.post('/quotes.json', this.state.quote).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/')

        });
    };

    handleQuoteChange = event => {
        event.persist();
        this.setState(prevState=> {
            return {
                quote: {...prevState.quote, [event.target.name]: event.target.value }
            };
        });

    };




    render () {
        return (
            <form className="add">
                <div>Category</div>
                <textarea  name="text" placeholder="enter quote..." value={this.state.quote.text} onChange={this.handleQuoteChange} />
                <input type="text" name="author" placeholder="author" value={this.state.quote.author} onChange={this.handleQuoteChange} />

                <button onClick={this.quoteSubmitHandler}>Submit</button>
            </form>
        );
    }

}

export default AddQuote;