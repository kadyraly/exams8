import React, {Component} from 'react';
import './Header.css';
import {NavLink, Route, Switch} from "react-router-dom";
import QuotesList from "../NavBar/QuotesList/QuotesList";
import AddQuote from "./AddQuote/AddQuote";


class Header extends Component {
    render () {
        return (
            <div className="Header">
                <ul>
                    <li><NavLink activeClassName="selected" exact to="/">Quotes</NavLink></li>
                    <li><NavLink activeClassName="selected" exact to="/add-quote">Submit new quote</NavLink></li>
                </ul>
                <Switch>
                    <Route path="/"  exact component={QuotesList} />
                    <Route path="/add-quote"  component={AddQuote} />

                </Switch>
            </div>
        )
    }
}



export  default Header;