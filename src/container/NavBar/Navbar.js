import React, {Component} from 'react';
import {Link, Switch, Route} from "react-router-dom";
import './Navbar.css';

import StarWars from "../StarWars/StarWars";
import Motivational from "../Motivational/Motivational";
import Humour from "../Humour/Humour";
import FamousPeople from "../FamousPeople/FamousPeople";
import Saying from "../Saying/Saying";

class Nav extends Component{
    render () {
        return (
            <div className="Nav">
                <ul>
                    <li><Link  exact to="/star-wars">Star Wars</Link></li>
                    <li><Link  exact to="/famous-people">Famous people</Link></li>
                    <li><Link  exact to="/saying">Saying</Link></li>
                    <li><Link  exact to="/humour">Humour</Link></li>
                    <li><Link  exact to="/motivation">Motivational</Link></li>

                </ul>
                <Switch>
                    <Route path="/star-wars" exact component={StarWars} />
                    <Route path="/famous-people"  component={FamousPeople} />
                    <Route path="/saying"  component={Saying} />
                    <Route path="/humour"  component={Humour} />
                    <Route path="/motivation"  component={Motivational} />
                </Switch>
            </div>
        )
    }
}


export  default Nav;