import React, {Component, Fragment} from 'react';
import './QuotesList.css';
import axios from '../../../axios';

import Desk from "../../../component/Desk/Desk";


class QuotesList extends Component {

    state = {
        quotes: []
    };

    componentDidMount() {
        this.getQuote();
    };
    getQuote=()=> {
        this.setState({loading: true});
        axios.get('/quotes.json').then((response) => {
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key});
            }
            this.setState({quotes})
        })
    };

    quoteDeleteHandler = id => {
        axios.delete(`/quotes/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes}
            })
        })
    };


    render () {
        return (
            <Fragment>
                <div className="home">

                        {this.state.quotes.map(quote => (<Desk key={quote.id} id={quote.id}
                        deleted={() => this.quoteDeleteHandler(quote.id)} />))}
                </div>
            </Fragment>
        );
    }

}

export default QuotesList;