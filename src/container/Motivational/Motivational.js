import React, {Component, Fragment} from 'react';
import './Motivational.css';
import axios from '../../axios';

class Motivational extends Component {
    state = {
        loadedQuote: null
    };
    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/quotes/${id}.json`).then((response) => {
            this.setState({loadedQuote: response.data})
        })
    };

    render () {
        if (this.state.loadedQuote) {
            return (
                <Fragment>
                    <h1>{this.state.loadedQuote.author}</h1>
                    <p>{this.state.loadedQuote.text}</p>
                </Fragment>
            )
        } else {
            return <p> load</p>
        }

    }
}
export  default Motivational;