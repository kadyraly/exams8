import React, {Component} from 'react';
import './Humour.css';
import axios from '../../axios';

class Humour extends Component {
    state = {
        loadedQuote: null
    };
    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/quotes/${id}.json`).then((response) => {
            this.setState({loadedQuote: response.data})
        })
    };

    render () {
        if (this.state.loadedQuote) {
            return (
                <div>
                    <h1>{this.state.loadedQuote.author}</h1>
                    <p>{this.state.loadedQuote.text}</p>
                </div>
            )
        }else {
            return <p> load</p>;
        }

    }
}
export  default Humour;