import  axios from 'axios';


const instance = axios.create({
    baseURL: 'https://exam8-79ed2.firebaseio.com/'

});

export default instance;