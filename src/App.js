import React, { Component, Fragment } from 'react';



import Header from "./container/Header/Header";
import Nav from "./container/NavBar/Navbar";




class App extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <Nav />


                <div className="footer"><p>Copyright © 2004 - 2018 Pluralsight LLC. All rights reserved</p></div>
            </Fragment>

        );
    }
}

export default App;
