import React from 'react';
import './Desk.css';

const Desk = props => (

    <div className="desk">
        <h4>{props.text}</h4>
        <h4>-{props.author}</h4>
        <button onClick={props.deleted}>x</button>
    </div>

);

export  default Desk;